/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/25 10:00:05 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/18 15:44:24 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NM_H
# define NM_H

# include <string.h>
# include <stdio.h>
# include <stdlib.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include "../libft/libft.h"
# define BUF_SIZE 10

typedef struct			s_symbol
{
	struct s_symbol		*next;
	char 				*name;
	char				symb_type;
	char				*symb_val;			
	int					addr_len;	
}						t_symbol;

void			ft_segment_commands_64(char *ptr, unsigned int i);

t_symbol		*ft_sort_list(t_symbol *list);
void			ft_print_output(t_symbol *list, int len);

char			*ft_fill_symb_val(char *symbol_val, int len);
char			ft_symbol_type(uint8_t n_type, uint64_t n_value);

int				ft_check_arg(char *filename);
void			ft_nm(char *ptr, unsigned int size_mmap);

void			ft_names_64(struct symtab_command *sym, char *ptr);
void			ft_names_32(struct symtab_command *sym, char *ptr);
void			ft_handle_64(char *ptr, unsigned int size_mmap);
void			ft_handle_32(char *ptr, unsigned int size_mmap);
void			ft_handle_fat(char *ptr);

#endif
