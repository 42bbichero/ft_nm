#!/bin/sh

make re -C ../
rm -f their mine mine_err their_err 2>/dev/null
TESTNM=1
TESTOTOOL=0
OPTION="-j"

if [ $TESTNM != 0 ]; then
	echo "Test nm "$OPTION" 32bits on 32/:"
	find "./32" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		[ $? == 1 ] && exit 0;
		FILE1=$filename
		nm $OPTION $FILE1 > their ; ../ft_nm $OPTION $FILE1 > mine
		COUNT=$(diff their mine | wc -l)
		if [ $COUNT != 0 ]; then
			echo "32bits test nm "$OPTION" : FAILED! : "$FILE1": see their, mine."
			exit 1
		else
			echo "32bits test nm "$OPTION" : SUCCESS! : "$FILE1
		fi
	done
	[ $? == 1 ] && exit 0;
	#without fat files
	echo "Test nm "$OPTION" 64bits binary on /bin:"
	find "/bin" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		[ $? == 1 ] && exit 0;
		if [ "$FILE1" != "/bin/bash" -a "$FILE1" != "/bin/sh" -a "$FILE1" != "/bin/sync" ]; then
			nm $OPTION $FILE1 > their ; ../ft_nm $OPTION $FILE1 > mine
			COUNT=$(diff their mine | wc -l)
			if [ $COUNT != 0 ]; then
				echo "64bits tests nm "$OPTION": FAILED! : "$FILE1": see their, mine."
				exit 1
			else
				echo "64bits tests nm "$OPTION": SUCCESS! :"$FILE1
			fi
		fi
	done
	[ $? == 1 ] && exit 0;
	echo "Test nm "$OPTION" fat files on /bin:"
	nm $OPTION /bin/bash > their ; ../ft_nm $OPTION /bin/bash > mine
	COUNT=$(diff their mine | wc -l)
	if [ $COUNT != 0 ]; then
		echo "Fat tests nm "$OPTION": FAILED! : /bin/bash: see their, mine."
		exit 1
	else
		echo "Fat tests nm "$OPTION": SUCCESS! : /bin/bash"
	fi
	[ $? == 1 ] && exit 0;
	nm $OPTION /bin/sh > their ; ../ft_nm $OPTION /bin/sh > mine
	COUNT=$(diff their mine | wc -l)
	if [ $COUNT != 0 ]; then
		echo "Fat tests nm "$OPTION": FAILED! : /bin/sh: see their, mine."
		exit 1
	else
		echo "Fat tests nm "$OPTION": SUCCESS! : /bin/sh"
	fi
	[ $? == 1 ] && exit 0;
	nm $OPTION /bin/sync > their ; ../ft_nm $OPTION /bin/sync > mine
	COUNT=$(diff their mine | wc -l)
	if [ $COUNT != 0 ]; then
		echo "Fat tests: FAILED! : /bin/sync: see their, mine."
		exit 1
	else
		echo "Fat tests: SUCCESS! : /bin/sync"
	fi
	[ $? == 1 ] && exit 0;
	echo "Test nm "$OPTION" archives 32 & 64:"
	find "./ar" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		nm $OPTION $FILE1 > their ; ../ft_nm $OPTION $FILE1 > mine
		COUNT=$(diff their mine | wc -l)
		if [ $COUNT != 0 ]; then
			echo "Archive tests nm "$OPTION": FAILED! : "$FILE1": see their, mine."
			exit 1
		else
			echo "Archive tests nm "$OPTION": SUCCESS! :"$FILE1
		fi
	done

	#test on generic lib, some ignored
	#with cat to ignore error due to nm sort order with same string
	# libkmodc++ ,cplus_start.o): cplus_stop.o): return 2 error : no name list, mine : archive size detected == 0, no entry found in archive
	# erreur /usr/lib/libnetsnm p.5.2.1.dylib detecte comme i386, nm /usr/lib/libnetsnm p.5.2.1.dylib(link editor): ???
	[ $? == 1 ] && exit 0;
	echo "Test nm "$OPTION" on /usr/lib:"
	find "/usr/lib" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		[ $? == 1 ] && exit 0;
		if [ "$FILE1" != "" -a "$FILE1" != "/usr/lib/libnetsnm p.5.2.1.dylib" -a "$FILE1" != "/usr/lib/libsqlite3.dylib" ]; then
			nm $OPTION $FILE1 2>/dev/null > their 
			../ft_nm $OPTION  $FILE1 2>/dev/null > mine
			COUNT=$(cat their mine | sort | uniq -u | wc -l)
			if [ $COUNT != 0 ]; then
				echo "/usr/lib tests nm "$OPTION": FfAILED! : "$FILE1": see their, mine."
				exit 1;
			else
				echo "/usr/lib tests nm "$OPTION": SUCCESS! :"$FILE1
			fi
		fi
	done

	[ $? == 1 ] && exit 0;
	#access denied sur /sbin/yubikey_shell
	echo "Test nm "$OPTION" on /sbin:"
	find "/sbin" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		[ $? == 1 ] && exit 0;
		if [ "$FILE1" != "/usr/lib/libnetsnm p.5.2.1.dylib" ]; then
			nm $OPTION $FILE1 2> their_err  > their  
			../ft_nm $OPTION $FILE1  2> mine_err > mine
			COUNT=$(cat their mine | sort | uniq -u | wc -l)
			COUNTET=$(cat their_err | grep "Permission denied" | wc -l)
			COUNTEM=$(cat mine_err | grep "Permission denied" | wc -l)
			if [ $COUNT != 0 ]; then
				echo "/sbin tests nm "$OPTION": FAILED! : "$FILE1": see their, mine."
				exit 1
			else
				echo "/sbin tests nm "$OPTION": SUCCESS! :"$FILE1
			fi
			if [ $COUNTET != $COUNTEM ]; then
				exit 1
			fi
		fi
	done
	[ $? == 1 ] && exit 0;
	echo "Test nm "$OPTION" on /usr/bin:"
	find "/usr/bin" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		nm $OPTION $FILE1 2> their_err  > their  
		../ft_nm $OPTION $FILE1  2> mine_err > mine
		COUNT=$(cat their mine | sort | uniq -u | wc -l)
		if [ $COUNT != 0 ]; then
			echo "/usr/bin tests nm "$OPTION": FAILED! : "$FILE1": see their, mine."
			exit 1
		else
			echo "/usr/bin tests nm "$OPTION": SUCCESS! :"$FILE1
		fi
	done

	echo "Test nm "$OPTION" on /usr/sbin:"
	find "/usr/sbin" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		[ $? == 1 ] && exit 0;
		FILE1=$filename
		nm $OPTION $FILE1 2> their_err  > their  
		../ft_nm $OPTION $FILE1  2> mine_err  > mine
		COUNT=$(cat their mine | sort | uniq -u | wc -l)
		if [ $COUNT != 0 ]; then
			echo "/usr/sbin tests nm "$OPTION": FAILED! : "$FILE1": see their, mine."
			exit 1
		else
			echo "/usr/sbin tests nm "$OPTION": SUCCESS! :"$FILE1
		fi
	done
fi

if [ $TESTOTOOL != 0 ]; then
	echo "Test OTOOL "$OPTION" 32bits on 32/:"
	find "./32" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		[ $? == 1 ] && exit 0;
		FILE1=$filename
		otool $OPTION $FILE1 > their ; ../ft_otool $OPTION $FILE1 > mine
		COUNT=$(diff their mine | wc -l)
		if [ $COUNT != 0 ]; then
			echo "32bits test OTOOL "$OPTION": FAILED! : "$FILE1": see their, mine."
			exit 1
		else
			echo "32bits test OTOOL "$OPTION": SUCCESS! : "$FILE1
		fi
	done
	[ $? == 1 ] && exit 0;
	#without fat files
	echo "Test OTOOL "$OPTION" 64bits binary on /bin:"
	find "/bin" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		[ $? == 1 ] && exit 0;
		if [ "$FILE1" != "/bin/bash" -a "$FILE1" != "/bin/sh" -a "$FILE1" != "/bin/sync" ]; then
			otool $OPTION $FILE1 > their ; ../ft_otool $OPTION $FILE1 > mine
			COUNT=$(diff their mine | wc -l)
			if [ $COUNT != 0 ]; then
				echo "Standard tests OTOOL "$OPTION": FAILED! : "$FILE1": see their, mine."
				exit 1
			else
				echo "Standard tests OTOOL "$OPTION": SUCCESS! :"$FILE1
			fi
		fi
	done
	[ $? == 1 ] && exit 0;
	echo "Test OTOOL "$OPTION" fat files on /bin:"
	otool $OPTION /bin/bash > their ; ../ft_otool $OPTION /bin/bash > mine
	COUNT=$(diff their mine | wc -l)
	if [ $COUNT != 0 ]; then
		echo "Fat tests OTOOL "$OPTION": FAILED! : /bin/bash: see their, mine."
		exit 1
	else
		echo "Fat tests OTOOL "$OPTION": SUCCESS! : /bin/bash"
	fi
	[ $? == 1 ] && exit 0;
	otool $OPTION /bin/sh > their ; ../ft_otool $OPTION /bin/sh > mine
	COUNT=$(diff their mine | wc -l)
	if [ $COUNT != 0 ]; then
		echo "Fat tests OTOOL "$OPTION": FAILED! : /bin/sh: see their, mine."
		exit 1
	else
		echo "Fat tests OTOOL "$OPTION": SUCCESS! : /bin/sh"
	fi
	[ $? == 1 ] && exit 0;
	otool $OPTION /bin/sync > their ; ../ft_otool $OPTION /bin/sync > mine
	COUNT=$(diff their mine | wc -l)
	if [ $COUNT != 0 ]; then
		echo "Fat tests OTOOL "$OPTION": FAILED! : /bin/sync: see their, mine."
		exit 1
	else
		echo "Fat tests OTOOL "$OPTION": SUCCESS! : /bin/sync"
	fi
	[ $? == 1 ] && exit 0;
	echo "Test OTOOL "$OPTION" archives 32 & 64:"
	find "./ar" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		otool $OPTION $FILE1 > their ; ../ft_otool $OPTION $FILE1 > mine
		COUNT=$(diff their mine | wc -l)
		if [ $COUNT != 0 ]; then
			echo "Archive tests OTOOL "$OPTION": FAILED! : "$FILE1": see their, mine."
			exit 1
		else
			echo "Archive tests OTOOL "$OPTION": SUCCESS! :"$FILE1
		fi
	done

	#test on generic lib, some ignored
	#with cat to ignore error due to nm sort order with same string
	# libkmodc++ ,cplus_start.o): cplus_stop.o): return 2 error : no name list, mine : no entry foud in archive
	# erreur /usr/lib/libnetsnm p.5.2.1.dylib detecte comme i386, nm ???
	[ $? == 1 ] && exit 0;
	echo "Test OTOOL "$OPTION" on /usr/lib:"
	find "/usr/lib" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		[ $? == 1 ] && exit 0;
		if [ "$FILE1" != "/usr/lib/libkmodc++.a" -a "$FILE1" != "/usr/lib/libnetsnm p.5.2.1.dylib" -a "$FILE1" != "/usr/lib/libsqlite3.dylib" ]; then
			otool $OPTION $FILE1 2>/dev/null > their 
			../ft_otool $OPTION $FILE1 2>/dev/null > mine
			COUNT=$(cat their mine | sort | uniq -u | wc -l)
			if [ $COUNT != 0 ]; then
				echo "/usr/lib tests OTOOL "$OPTION": FAILED! : "$FILE1": see their, mine."
				exit 1;
			else
				echo "/usr/lib tests OTOOL "$OPTION": SUCCESS! :"$FILE1
			fi
		fi
	done

	[ $? == 1 ] && exit 0;
	#access denied sur /sbin/yubikey_shell
	echo "Test OTOOL "$OPTION" on /sbin:"
	find "/sbin" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		[ $? == 1 ] && exit 0;
		if [ "$FILE1" != "/usr/lib/libnetsnm p.5.2.1.dylib" ]; then
			otool $OPTION $FILE1 2> their_err > their  
			../ft_otool $OPTION $FILE1  2> mine_err  > mine
			COUNT=$(cat their mine | sort | uniq -u | wc -l)
			COUNTET=$(cat their_err | grep "Permission denied" | wc -l)
			COUNTEM=$(cat mine_err | grep "Permission denied" | wc -l)
			if [ $COUNT != 0 ]; then
				echo "/sbin tests OTOOL "$OPTION": FAILED! : "$FILE1": see their, mine."
				exit 1
			else
				echo "/sbin tests OTOOL "$OPTION": SUCCESS! :"$FILE1
			fi
			if [ $COUNTET != $COUNTEM ]; then
				exit 1
			fi
		fi
	done
	[ $? == 1 ] && exit 0;
	echo "Test on /usr/bin:"
	find "/usr/bin" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		FILE1=$filename
		otool $OPTION $FILE1 2> their_err  > their  
		../ft_otool $OPTION $FILE1  2> mine_err  > mine
		COUNT=$(cat their mine | sort | uniq -u | wc -l)
		if [ $COUNT != 0 ]; then
			echo "/usr/bin tests OTOOL "$OPTION": FAILED! : "$FILE1": see their, mine."
			exit 1
		else
			echo "/usr/bin tests OTOOL "$OPTION": SUCCESS! :"$FILE1
		fi
	done

	echo "Test OTOOL "$OPTION" on /usr/sbin:"
	find "/usr/sbin" -type f -print0 | \
	while IFS='' read -r -d '' filename; do
		[ $? == 1 ] && exit 0;
		FILE1=$filename
		otool $OPTION $FILE1 2> their_err  > their  
		../ft_otool $OPTION $FILE1  2> mine_err  > mine
		COUNT=$(cat their mine | sort | uniq -u | wc -l)
		if [ $COUNT != 0 ]; then
			echo "/usr/sbin tests OTOOL "$OPTION": FAILED! : "$FILE1": see their, mine."
			exit 1
		else
			echo "/usr/bin tests OTOOL "$OPTION": SUCCESS! :"$FILE1
		fi
	done
fi
rm -f their mine mine_err their_err 2>/dev/null





