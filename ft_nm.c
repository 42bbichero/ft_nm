/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/17 09:12:03 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/18 16:16:45 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**				---------------------------------
**				|			 HEADER				|
**				|  ---------------------------  |
**				|  |		LOAD COMMANDS	 |	|
**				|  ---------------------------  |
**				|  |   SEGMENT LOAD COMMAND  |  |
**				|  ---------------------------  |
**				---------------------------------
**				|			  DATA				|
**				|  ---------------------------	|
**				|  |		SECTION 1		 |	|
**				|  | ----------------------- |  |
**				|  | |		SEGMENT 1	   | |	|
**				|  | ----------------------- |  |
**				|  | |		SEGMENT 2	   | |	|
**				|  | ----------------------- |	|
**				|  ---------------------------  |
**				|  |		SECTION 1		 |	|
**				|								|
**				---------------------------------
**
**
** Sections:
**		__PAGEZERO : (no right access) size of full VM page of system
**		__TEXT : code to run (executable mode only)
**				 __stub_helper and __stub use for dynamic linker (dyld)
**		__DATA : value need to be update (Read / write)
**		__LINKEDIT : segment contains raw data used by the dynamic
**					 linker, such as symbol, string, and relocation
**					 table entries
*/

#include "nm.h"

/*
** ft_handle_fat(), is call for parse fat binary header and arch,
** ...
*/
void		ft_handle_fat(char *ptr)
{
	struct fat_header	*header;
	struct fat_arch		*arch;
	unsigned int		i;
	unsigned int magic_number;

	header = (struct fat_header *)ptr;
	arch = (void *)ptr;
	i = 0;
	while (i++ < header->nfat_arch)
	{
		ft_putstr("offset :");
		printf("%u\n", arch->offset);
		ft_putstr("size   :");
		printf("%u\n", arch->size);
		ft_putstr("align  :");
		printf("%u\n", arch->align);
		magic_number = *(int *)arch;

		printf("ptr addr:     %p\n", arch);fflush(stdout);
		printf("magic number: %d\n", *(int *)arch);fflush(stdout);
		printf("magic.h :     %d\n", MH_MAGIC_64);fflush(stdout);

		if (magic_number == MH_MAGIC_64)
			ft_handle_64(ptr, 10000);
		else if (magic_number == MH_MAGIC)
			ft_handle_32(ptr, 10000);
		else
			arch = (void *)arch + sizeof(arch);
	}
}

/*
** ft_handle_32(), read header of 32bit binary
** read each instructions
*/
void		ft_handle_32(char *ptr, unsigned int size_mmap)
{
	int 					ncmds;
	int						i;
	struct mach_header	*header;
	struct load_command		*lc;
	struct symtab_command	*sym;

	i = 0;
	header = (struct mach_header *)ptr;
	ncmds = header->ncmds;
	lc = (void *)ptr + sizeof(*header);
	ft_putendl("Enter in ft_handle_32");
	while (i < ncmds && lc->cmdsize >= 8)
	{
		ft_putstr("load command size: ");
		ft_putnbrel(lc->cmdsize);
		if (lc->cmd == LC_SEGMENT_64)
			ft_segment_commands_64(ptr, i);
		else if (lc->cmd == LC_SYMTAB)
		{
			sym = (struct symtab_command *)lc;
			if (size_mmap > sym->stroff)
				ft_names_32(sym, ptr);
			break ;
		}
		lc = (void *)lc + lc->cmdsize;
		i++;
	}
}

/*
** ft_handle_64(), read header of 32bit binary
** read each instructions
*/
void		ft_handle_64(char *ptr, unsigned int size_mmap)
{
	int 					ncmds;
	int						i;
	struct mach_header_64	*header;
	struct load_command		*lc;
	struct symtab_command	*sym;

	i = 0;
	header = (struct mach_header_64 *)ptr;
	ncmds = header->ncmds;
	lc = (void *)ptr + sizeof(*header);

	/*
	   ft_putstr("sizeof(*header): ");
	   ft_putendl(ft_itoa(sizeof(*header)));
	   ft_putstr("magic      : ");
	   ft_putendl(ft_itoa(header->magic));
	   ft_putstr("cputype    : ");
	   ft_putendl(ft_itoa((int)header->cputype));
	   ft_putstr("filetype   : ");
	   ft_putendl(ft_itoa(header->filetype));
	   ft_putstr("ncmds      : ");
	   ft_putendl(ft_itoa(header->ncmds));
	   ft_putstr("sizeofcmds : ");
	   ft_putendl(ft_itoa(header->sizeofcmds));
	   ft_putstr("flags      : ");
	   ft_putendl(ft_itoa(header->flags));
	   */
	while (i < ncmds)
	{
		printf("ncmds (load commands): %d\n", ncmds);fflush(stdout);
		printf("lc->cmd:               %d\n", lc->cmd);fflush(stdout);
		printf("lc->cmdsize:           %d\n", lc->cmdsize);fflush(stdout);

		if (lc->cmd == LC_SEGMENT_64)
			ft_segment_commands_64(ptr, i);
		else if (lc->cmd == LC_SYMTAB)
		{
			sym = (struct symtab_command *)lc;
			printf("sym->strsize:           %d\n", sym->strsize);fflush(stdout);
			if (size_mmap > sym->stroff)
				ft_names_64(sym, ptr);
			break ;
		}
		lc = (void *)lc + lc->cmdsize;
		i++;
	}
}

/*
** fill t_symbol struct with symbol name and value
** sort list by ASCII order and print to output
*/
void		ft_names_32(struct symtab_command *sym, char *ptr)
{
	char 			*stringtable;
	struct nlist	*array;
	t_symbol		*list;
	t_symbol		*start;
	int				i;

	list = (t_symbol *)malloc(sizeof(t_symbol));
	start = list;
	array = (void *)ptr + sym->symoff;
	stringtable = (void *)ptr + sym->stroff;

	printf("nsyms:  %d\n", sym->nsyms);fflush(stdout);
	printf("symoff: %d\n", sym->symoff);fflush(stdout);
	printf("stroff: %d\n", sym->stroff);fflush(stdout);

	i = 0;
	while (i < (int)sym->nsyms)
	{
		if (sym->strsize < array[i].n_un.n_strx)
		{
			ft_putendl("bad string index");
			break ;
		}
		list->name = stringtable + array[i].n_un.n_strx;
		list->symb_val = ft_fill_symb_val(ft_itohex(array[i].n_value, 8), 8);
		list->symb_type = ft_symbol_type(array[i].n_type, array[i].n_value);
		if (i++ + 1 == (int)sym->nsyms)
			list->next = NULL;
		else
			list->next = (t_symbol *)malloc(sizeof(t_symbol));
		list = list->next;
	}
	if (start->next)
		ft_print_output(start, 8);
}

/*
** fill t_symbol struct with symbol name and value
** sort list by ASCII order and print to output
*/
void		ft_names_64(struct symtab_command *sym, char *ptr)
{
	char 			*stringtable;
	struct nlist_64 *array;
	t_symbol		*list;
	t_symbol		*start;
	int				i;

	list = (t_symbol *)malloc(sizeof(t_symbol));
	start = list;
	/*
	   printf("nsyms:   %d\n", sym->nsyms);fflush(stdout);
	   printf("symoff:  %d\n", sym->symoff);fflush(stdout);
	   printf("stroff:  %d\n", sym->stroff);fflush(stdout);
	   printf("strsize: %d\n", sym->strsize);fflush(stdout);
	   */
	array = (void *)ptr + sym->symoff;
	stringtable = (void *)ptr + sym->stroff;
	i = 0;
	while (i < (int)sym->nsyms)
	{
		/*
		   ft_putstr("stringtable: ");
		   ft_putendl(stringtable);
		   ft_putstr("n_strx: ");
		   ft_putnbrel(array[i].n_un.n_strx);
		   */
		if (sym->strsize < array[i].n_un.n_strx)
		{
			ft_putendl("bad string index");
			break ;
		}
		list->name = stringtable + array[i].n_un.n_strx;
		list->symb_val = ft_fill_symb_val(ft_itohex(array[i].n_value, 8), 16);
		list->symb_type = ft_symbol_type(array[i].n_type, array[i].n_value);

		ft_putendl(stringtable + array[i].n_un.n_strx);
		ft_putstr("\ttype: ");
		ft_putstr(ft_itoa(array[i].n_type));
		ft_putstr("  char symbol: ");
		ft_putchar(list->symb_type);
		ft_putchar('\n');
		ft_putstr("\tsect: ");
		ft_putendl(ft_itoa(array[i].n_sect));
		ft_putstr("\tdesc: ");
		ft_putendl(ft_itoa(array[i].n_desc));
		ft_putstr("\tvalue: ");
		ft_putendl(ft_itoa(array[i].n_value));
		ft_putstr("\thex value: ");
		ft_putendl(ft_itohex(array[i].n_value, 8));

		if (i++ + 1 == (int)sym->nsyms)
			list->next = NULL;
		else
			list->next = (t_symbol *)malloc(sizeof(t_symbol));
		list = list->next;
	}
	if (start->next)
		ft_print_output(start, 16);
}
