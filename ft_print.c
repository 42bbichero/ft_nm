/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/16 14:39:12 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/17 16:10:03 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"


/*
** ft_print_output(), print synbol value, type and name
** to STDIN
*/
void			ft_print_output(t_symbol *list, int len)
{
	int			i;

	i = 0;
	list = ft_sort_list(list);
	while (list)
	{
		i = len;
		if (list->symb_type == '0')
			ft_putstr("ERR");
		else if (list->symb_type == 'T')
			ft_putstr(list->symb_val);
		else
		{
			while (i-- > 0)
				ft_putchar(' ');
		}
		ft_putchar(' ');
		ft_putchar(list->symb_type);
		ft_putchar(' ');
		ft_putstr(list->name);
		if (list->next)
			ft_putchar('\n');
		list = list->next;
	}
	ft_putchar('\n');
}

/*
** ft_sort_list(), sors symbol name by ASCII
*/
t_symbol			*ft_sort_list(t_symbol *list)
{
	t_symbol *tmp;
	t_symbol *start;
	t_symbol *prev;
	t_symbol *next;

	start = list;
	prev = NULL;
	while (list && list->next)
	{
		if (ft_strcmp(list->name, list->next->name) > 0)
		{
			if (list == start)
				start = list->next;	
			tmp = list->next;
			next = tmp->next;
			if (prev)
				prev->next = tmp;
			list->next = list;
			if (next)
				list->next = next;
			else
				list->next = NULL;
			tmp->next = list;
			list = start;
		}
		else
		{
			prev = list;
			list = list->next;
		}
	}
	return (start);
}
