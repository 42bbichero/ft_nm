#include <stdio.h>

int main(int ac, char **av)
{
	printf("number of argument: %d\nname of executable: %s\n", ac, av[0]);fflush(stdout);

	char *str = "yolo";
	int i = *(int *)str;

	printf("str addr     : %p\n", str);
	printf("str *int addr: %p\n", (int *)str);
	printf("     i addr  : %d\n", i);
	return (0);
}
