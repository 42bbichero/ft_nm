/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_symbol.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 21:45:57 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/18 09:00:22 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

/*
** Apply bit mask of N_TYPE to get the symbol value
** and return the char corresponding
*/
char			ft_symbol_type(uint8_t n_type, uint64_t n_value)
{
	int	res;

	res = n_type & N_TYPE;
	if (res == N_ABS)
		return ('A');
	else if (res == N_UNDF)
	{
		if (n_value != 0)
			return ('C');
		return ('U');
	}
	else if (res == N_INDR)
		return ('I');
	else if (res == N_SECT)
		return ('T');
	else if (res == N_PBUD)
		return ('U');
	else
		return ('S');
	return ('0');
}

/*
** ft_fill_symb_val(), fill start of string with '0' and
** copy symbol_val to the end if symbol_val
** is shorter than len
*/
char			*ft_fill_symb_val(char *symbol_val, int len)
{
	char		*value;
	char		*start;
	int			diff;

	diff = len - ft_strlen(symbol_val);
	if (diff > 0)
	{
		value = (char *)malloc(sizeof(char) * len + 1);
		start = value;
		while (diff > 0)
		{
			*value++ = '0';
			diff--;
		}
		value = ft_strcpy(value, symbol_val);
	}
	else
		start = symbol_val;
	return (start);
}
