/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_segments.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/18 14:46:15 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/18 15:54:29 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

void			ft_segment_commands_64(char *ptr, unsigned int i)
{
	struct segment_command_64 *segment_c;

	ft_putstr("i : ");
	ft_putnbrel(i);
	segment_c = (void *)ptr + sizeof(struct mach_header_64) + (sizeof(struct segment_command_64) * i);
	ft_putstr("segment name: ");
	ft_putendl(segment_c->segname);
}
