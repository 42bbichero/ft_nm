/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_main.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/25 11:50:40 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/18 13:06:19 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

/*
** MH_MAGIC_64 - 0xffffffff = magic_number
** array = ptr + symbol table offset
** stringtable = ptr + string table offset
** function name = array[i].index_into_the_string_table
** nsyms = number of symbol table entries
** symoff = symbol tale offset
** stroff = string table offset
** For 64 bit binary file, 16bit by line (hexdump)
** sizeof(*header) = 32bit (2 lines)
** TODO: sort by address (same symbol name)
**
** U : undefine
** A : absolute
** T : text section symbol
** D : Data section symbol
** B : bss section symbol
** C : common symbol
** S : symbol is section other than those above
** I : indirect symbol
*/

/*
** ft_check_arg() function, check argument
** given, return 0 or 1, and print error
** message
*/

int				ft_check_arg(char *filename)
{
	int 		fd;
	void 		*ptr;
	struct stat	buf;

	ptr = NULL;
	if ((fd = open(filename, O_RDONLY)) > 0)
	{
		if (fstat(fd, &buf) >= 0)
		{
			ft_putstr("mmap call with size: ");
			ft_putnbrel(buf.st_size);
			if ((ptr = mmap(0, buf.st_size, PROT_READ, MAP_PRIVATE, fd, 0))
																!= MAP_FAILED)
			{
				ft_nm(ptr, buf.st_size);
				munmap(ptr, buf.st_size);
			}
			else
				ft_putendl("ERR: mmap failed");
		}
		close(fd);
		return (1);
	}
	else
	{
		ft_putstr("nm: ");
		ft_putstr(filename);
		ft_putendl(": No such file or directory.");
	}
	return (0);
}

void			ft_nm(char *ptr, unsigned int size_mmap)
{
	unsigned int magic_number;
	magic_number = *(int *)ptr;

	printf("ptr addr:     %p\n", ptr);fflush(stdout);
	printf("magic number: %d\n", *(int *)ptr);fflush(stdout);
	printf("magic.h :     %d\n", MH_MAGIC_64);fflush(stdout);

	if (magic_number == MH_MAGIC_64)
		ft_handle_64(ptr, size_mmap);
	else if (magic_number == MH_MAGIC)
		ft_handle_32(ptr, size_mmap);
	else if (magic_number == FAT_MAGIC || magic_number == FAT_CIGAM)
		ft_handle_fat(ptr);
}	

/*
** main() function, check valability of arguments
** possible cases :
** - if no argument given, no a.out in current dir
** - no such file or directory
** - invalid binary file
** - ...
*/

int				main(int ac, char **av)
{
	int 	i;

	i = 1;
	// add a.out to handle argument like main have been call av[]
	if (ac == 1)
		av[i] = "a.out";

	// check all argument even if one failed
	while (av[i])
	{
		// return 1 if correct binary file
		if (ft_check_arg(av[i]) < 0)
			ft_putendl("Error");
		if (av[i + 1])
			ft_putchar('\n');
		i++;
	}
	return (0);
}
